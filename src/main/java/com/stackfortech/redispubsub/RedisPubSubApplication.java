package com.stackfortech.redispubsub;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.stackfortech.redispubsub.configuration.RedisMessagePublisher;

@SpringBootApplication
public class RedisPubSubApplication {
	public static void main(String[] args) {
		SpringApplication.run(RedisPubSubApplication.class, args);
	}

}
