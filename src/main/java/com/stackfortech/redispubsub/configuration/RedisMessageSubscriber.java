package com.stackfortech.redispubsub.configuration;

import org.redisson.Redisson;
import org.redisson.api.RTopic;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RedisMessageSubscriber implements MessageListener {

    public static List<String> messageList = new ArrayList<>();
//    RedisConfiguration template;
//    @Autowired
//    private RedisConnectionFactory redisConnectionFactory;
//    public ChannelTopic topic = new ChannelTopic("topic22");  
//    private RedisMessagePublisher messagePublisher = new RedisMessagePublisher(template.redisTemplate(redisConnectionFactory),topic);
    
    @Override
    public void onMessage(Message message, byte[] bytes) {
        messageList.add(message.toString());
        System.out.println("Message received : " + message.toString());
        publicar(message);
    }
    
	static void publicar(Message message) {
		RedissonClient client = Redisson.create();
		RTopic topic = client.getTopic("topicnn");
		topic.publish(message.toString());
	}

}
